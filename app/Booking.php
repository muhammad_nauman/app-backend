<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $guarded = [];

    public function consumer()
    {
        return $this->belongsTo(Consumer::class);
    }

    public function service_provider()
    {
        return $this->belongsTo(ServiceProvider::class);
    }

    public function booking_details()
    {
        return $this->hasOne(BookingDetail::class);
    }

    protected $casts = [
        'category_id' => 'integer',
        'sub_category_id' => 'integer',
        'job_type' => 'integer',
        'consumer_id' => 'integer',
    ];

    public function getMediaAttribute($value) {
        return json_decode($value);
    }
}
