<?php

namespace App\Http\Controllers\Auth;

use App\Category;
use App\Consumer;
use App\ServiceProvider;
use App\User;
use App\Http\Controllers\Controller;
use App\Vendor;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    public $user;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if($this->validator($request->all())->fails()) {
            return response()->json($this->validator($request->all())->errors());
        }
        event(new Registered($this->user = $this->create($request->except('password_confirmation', 'app'))));
        if(request('app') === 'consumer') {
            $this->user->consumer()->save(new Consumer());
        }
        if(request('app') === 'vendor') {
            $this->user->vendor()->save(new Vendor());
        }
        if(request('app') === 'service_provider') {
            $this->user->service_provider()->save(new ServiceProvider());
        }
        $this->user->{request('app')};
        if (! $token = JWTAuth::fromUser($this->user)) {
            return ['error' => 'Unauthorized'];
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'number' => ['required', 'string', 'min:6', 'max:20'],
            'app' => ['required', 'in:consumer,service_provider,vendor'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create(array_merge($data, ['password' => bcrypt($data['password'])]));
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => $this->user,
            'categories' => Category::with('sub_categories')->get()
        ]);
    }
}
