<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingDetail;
use App\ServiceProvider;
use App\SubCategory;
use App\Vendor;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'success' => true,
            'bookings' => Booking::with('consumer', 'service_provider')->paginate(request('per_page', 15))
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        $rules = [
            'category_id' => ['required', 'exists:categories,id'],
            'sub_category_id' => ['sometimes', 'exists:sub_categories,id'],
            'latitude' => ['required'],
            'longitude' => ['required'],
        ];
        if(request('booking_type') == 1) {
            $this->validate(request(), $rules);
            $vendors = Vendor::whereHas('sub_categories', function($query) {
                $query->where('sub_category_id', request('sub_category_id'));
            })->get();
            
            return response()->json([
                'success' => true,
                'data' => $vendors
            ], 200);
        }
        $rules = array_merge($rules, [
            'date_time' => ['required'],
            'description' => 'required',
            'media' => 'sometimes|array',
            'media.*' => 'image',
            'job_type' => 'required',
        ]);
        $this->validate(request(), $rules);

        $media = [];

        if(request()->has('media')) {
            collect(request('media'))->map(function($singleMedia) use (&$media) {
                array_push($media, $singleMedia->storeAs('/bookings', time() . str_random(5) . '.png'));
            });
        }
        $data = request()->except('media', 'booking_type');
        $data = array_merge($data, ['media'=>json_encode($media)]);

        $booking = new Booking($data);

        request()->user()->consumer->bookings()->save($booking);

        $booking->service_providers = ServiceProvider::whereHas('sub_categories', function($query) {
            $query->where('sub_category_id', request('sub_category_id'));
        })->with('media', 'rating', 'rating.rated_by')->limit(10)->get();

        return response()->json([
            'success' => true,
            'data' => $booking,
        ], 201);
    }

    public function myOrders()
    {
        $orders = BookingDetail::whereNotNull('vendor_product_id')->get();
        return response()->json([
            'data' => $orders
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myBookings()
    {
        $consumer = optional(request()->user())->consumer;
        $bookings = optional($consumer)->bookings()->with('booking_details')->get();
        return response()->json([
            'success' => true,
            'bookings' => $bookings,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }

    public function cancel(Booking $booking)
    {
        $booking->status = 4;
        $booking->save();

        return response()->json([
            'success' => true,
            'booking' => $booking
        ]);
    }
}
