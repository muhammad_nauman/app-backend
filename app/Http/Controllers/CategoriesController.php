<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;

class CategoriesController extends Controller
{
    public function create()
    {
        $this->validate(request(), [
            'name_en' => 'required|unique:categories',
            'name_fr' => 'required|unique:categories',
            'name_ar' => 'required|unique:categories',
            'asset_url_en' => 'required',
            'asset_url_fr' => 'required',
            'asset_url_ar' => 'required',
        ]);

        request()->request->add(['name_ur' => request('name_ar')]);
        request()->request->add(['asset_url_ur' => request('asset_url_ar')]);

        $category = Category::create(
            array_merge(
                compact('asset_url_ar', 'asset_url_en', 'asset_url_fr', 'asset_url_ur'), request()->only('name_ur', 'name_en', 'name_fr', 'name_ar', 'asset_url_en', 'asset_url_fr', 'asset_url_ar', 'asset_url_ur')
            )
        );
        return response()->json([
            'success' => true,
            'category' => $category
        ], 201);

    }

    public function get()
    {
        return response()->json([
            'success' => true,
            'categories' => Category::with('sub_categories')->get(),
        ]);
    }

    public function createSubCategory(Category $category)
    {
        $this->validate(request(), [
            'name_en' => 'required|unique:categories',
            'name_fr' => 'required|unique:categories',
            'name_ar' => 'required|unique:categories',
            'asset_url_en' => 'required',
            'asset_url_fr' => 'required',
            'asset_url_ar' => 'required',
               ]);

        request()->request->add(['name_ur' => request('name_ar')]);
        request()->request->add(['asset_url_ur' => request('asset_url_ar')]);

        $sub_category = new SubCategory(
            array_merge(
                compact('asset_url_en', 'asset_url_ar', 'asset_url_fr', 'asset_url_ur'),  request()->only('name_ur', 'name_en', 'name_fr', 'name_ar', 'asset_url_en', 'asset_url_fr', 'asset_url_ar', 'asset_url_ur')
            )
        );

        $category->sub_categories()->save($sub_category);

        return response()->json([
            'success' => true,
            'sub_category' => $sub_category
        ], 201);
    }
}
