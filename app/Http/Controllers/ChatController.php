<?php

namespace App\Http\Controllers;

use App\Chat;
use Illuminate\Http\Request;
use App\Vendor;

class ChatController extends Controller
{
    public function send()
    {
        $this->validate(request(), [
            'receiver_id' => 'required|exists:users,id',
            'message_type' => 'required|in:0,1,2',
        ]);

        $chat = Chat::create(array_merge(['sender_id' => request()->user()->id], request()->all()));

        return response()->json([
            'success' => true,
            'data' => $chat
        ], 201);
    }

    public function getChat()
    {
        $user = request()->user();
        $chat = Chat::where(function($query) use ($user){
            $query->where('sender_id', $user->id)
                ->where('receiver_id', request('receiver_id'));
        })->orWhere(function($query) use ($user){
            $query->where('receiver_id', $user->id)
                ->where('sender_id', request('receiver_id'));
        })->with('sender', 'receiver')->get();
        return response()->json([
            'success' => true,
            'data' => $chat
        ]);
    }

    public function delete(Chat $chat)
    {
        $chat->delete();
        return response()->json([
            'success' => true
        ]);
    }


    public function lastMessages()
    {
        $user = request()->user();
        
        $data = \DB::table('chats')
                     ->where('chats.sender_id', '=', $user->id)
                     ->orderBy('chats.id' , 'desc')
                     ->groupBy('chats.receiver_id')->get();
        
        foreach ($data as $key => $value) {
            $data[$key]->vendor = Vendor::find($data[$key]->receiver_id);
        }


        return response()->json([
            'success' => true,
            'data' => $data
        ]);

    }

}
