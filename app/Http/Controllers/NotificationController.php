<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function get()
    {
        return response()->json([
            'success' => true,
            'data' => request()->user()->notifications
        ], 200);
    }

    public function read(Notification $notification)
    {
        $notification->read();
        return response()->json([
            'success' => true,
        ]);
    }

    public function delete(Notification $notification)
    {
        $notification->delete();
        return response()->json([
            'success' => true,
        ]);
    }
}
