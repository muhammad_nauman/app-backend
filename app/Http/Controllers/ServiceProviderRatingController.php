<?php

namespace App\Http\Controllers;

use App\service_provider_rating;
use App\ServiceProvider;
use App\ServiceProviderRating;
use Illuminate\Http\Request;

class ServiceProviderRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $this->validate(request(), [
            'satisfaction_rating' => 'required|max:5',
            'service_on_time_rating' => 'required|max:5',
            'service_on_budget_rating' => 'required|max:5',
            'feedback' => 'sometimes|max:255',
            'service_provider_id' => 'required|exists:service_providers,id'
        ]);

        $rating = new ServiceProviderRating(request()->all());

        request()->user()->service_providers_rating()->save($rating);
        return response()->json([
            'success' => true,
            'rating' => $rating
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\service_provider_rating  $service_provider_rating
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceProviderRating $service_provider_rating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\service_provider_rating  $service_provider_rating
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceProviderRating $service_provider_rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\service_provider_rating  $service_provider_rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceProviderRating $service_provider_rating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\service_provider_rating  $service_provider_rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceProviderRating $service_provider_rating)
    {
        //
    }
}
