<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceProvider extends Model
{
    protected $guarded = [];
    protected $appends = [
        'avg_rating',
        'is_open_now',
    ];

    protected $casts = [
        'contact_details' => 'array'
    ];

    function getAverageRatingAttribute(){
        return round($this->rating()->avg('ratings'),1);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function media()
    {
        return $this->hasMany(ServiceProviderMedia::class, 'service_provider_id');
    }

    public function rating()
    {
        return $this->hasMany(ServiceProviderRating::class, 'service_provider_id');
    }

    public function orders()
    {
        return $this->hasMany(Booking::class, 'service_provider_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function sub_categories()
    {
        return $this->belongsToMany(SubCategory::class);
    }

    public function GetAvgRatingAttribute()
    {
        return round(($this->rating()->avg('satisfaction_rating') + $this->rating()->avg('service_on_time_rating') + $this->rating()->avg('service_on_budget_rating')) / 3,1);
    }

    public function getIsOpenNowAttribute()
    {
        return (time() >= strtotime($this->open_time) && time() <= strtotime($this->close_time));
    }
}
