<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceProviderRating extends Model
{
    protected $table = 'service_provider_ratings';

    protected $guarded = [];

    public function service_provider()
    {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }

    public function rated_by()
    {
        return $this->belongsTo(User::class, 'rated_by');
    }
}
