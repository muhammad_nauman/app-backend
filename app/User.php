<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function vendor()
    {
        return $this->hasOne(Vendor::class);
    }

    public function service_provider()
    {
        return $this->hasOne(ServiceProvider::class);
    }

    public function consumer()
    {
        return $this->hasOne(Consumer::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function chat()
    {
        return $this->hasMany(Chat::class);
    }

    public function service_providers_rating()
    {
        return $this->hasMany(ServiceProviderRating::class, 'rated_by');
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
