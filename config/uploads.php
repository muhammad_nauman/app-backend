<?php

return [
    'category' => [
        'folder' => 'category',
        'public_relative' => 'category/',
        'path' => 'files/category',
        'full_path' => storage_path('app/category'),
        'rules' => [
            'mimes:jpeg,jpg,png,xlsx,xls,doc,docx,pdf',
            // 'max:' . 10*1024*1024,
        ],
    ],
    'sub_category' => [
        'folder' => 'sub_category',
        'public_relative' => 'sub_category/',
        'path' => 'files/sub_category',
        'full_path' => storage_path('app/sub_category'),
        'rules' => [
            'mimes:jpeg,jpg,png,xlsx,xls,doc,docx,pdf',
            // 'max:' . 10*1024*1024,
        ],
    ],
    'featured_profile' => [
        'folder' => 'featured_profile',
        'public_relative' => 'featured_profile/',
        'path' => 'files/featured_profile',
        'full_path' => storage_path('app/featured_profile'),
        'rules' => [
            'mimes:jpeg,jpg,png,xlsx,xls,doc,docx,pdf',
            // 'max:' . 10*1024*1024,
        ],
    ]
];