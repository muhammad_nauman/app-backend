<?php

use Faker\Generator as Faker;

$factory->define(App\Notification::class, function (Faker $faker) {
    return [
        'type' => array_random(['service', 'product']),
        'title' => $faker->word,
        'content' => $faker->sentence
    ];
});
