<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
    ];
});

$userTypes = ['consumer', 'vendor'];

$factory->afterCreating(App\User::class, function ($user, Faker $faker) use ($userTypes) {
    $createThis = array_random($userTypes);

    switch ($createThis) {
        case 'consumer': {
            \App\Consumer::create([
                'user_id' => $user->id
            ]);
            factory(App\Notification::class, 10)->make()->map(function ($notification) use ($user){
                $user->notifications()->save($notification);
            });
        }
        case 'vendor': {
            $vendor = \App\Vendor::create([
                'user_id' => $user->id,
                'name' => $faker->company,
                'owner_name' => $faker->name,
                'address' => $faker->address,
                'image' => $faker->imageUrl(),
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude,

            ]);
            \App\SubCategory::inRandomOrder()->limit(10)->get()->map(function($sub_category) use ($vendor) {
                \Illuminate\Support\Facades\DB::insert("Insert into sub_category_vendor (sub_category_id, vendor_id) values ({$sub_category->id}, {$vendor->id})");
            });

            \App\Category::inRandomOrder()->limit(3)->get()->map(function($category) use ($vendor) {
                    \Illuminate\Support\Facades\DB::insert("Insert into category_vendor (category_id, vendor_id) values ({$category->id}, {$vendor->id})");
            });
        }
    }
});