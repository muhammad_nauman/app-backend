<?php

use Faker\Generator as Faker;

$factory->define(App\VendorProduct::class, function (Faker $faker) {
    
    $images [] = [
        'url' => $faker->imageUrl(20, 20)
    ];

    return [
        'name' => $faker->name,
        'vendor_id' => \App\Vendor::inRandomOrder()->value('id'),
        'sku' => str_random(10),
        'description' => $faker->paragraph,
        'price' => random_int(99, 9999),
        'delivery_period' => array_random(['1-2 days', '3-4 days', '1-2 weeks']),
        'image' => $images
    ];
});

$factory->afterCreating(App\VendorProduct::class, function ($product, $faker) {
    \App\SubCategory::inRandomOrder()->limit(10)->get()->map(function($sub_category) use ($product) {
        \Illuminate\Support\Facades\DB::insert("Insert into sub_category_vendor_product (sub_category_id, vendor_product_id) values ({$sub_category->id}, {$product->id})");
    });
});

$factory->afterCreating(App\VendorProduct::class, function ($product, $faker) {
    \App\Category::inRandomOrder()->limit(3)->get()->map(function($category) use ($product) {
        \Illuminate\Support\Facades\DB::insert("Insert into category_vendor_product (category_id, vendor_product_id) values ({$category->id}, {$product->id})");
    });
});
