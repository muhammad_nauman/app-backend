<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en')->unique();
            $table->string('name_fr')->unique();
            $table->string('name_ar')->unique();
            $table->string('name_ur')->unique();
            $table->string('asset_url_en');
            $table->string('asset_url_fr');
            $table->string('asset_url_ar');
            $table->string('asset_url_ur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
