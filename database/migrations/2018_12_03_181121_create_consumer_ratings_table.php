<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumerRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumer_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rating');
            $table->unsignedInteger('booking_id')->index();
            $table->unsignedInteger('consumer_id')->index();
            $table->string('feedback')->index()->nullable();
            $table->timestamps();
        });
        Schema::table('consumer_ratings', function(Blueprint $table) {
            $table->foreign('booking_id')
                ->references('id')
                ->on('bookings')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('consumer_id')
                ->references('id')
                ->on('consumers')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumer_ratings');
    }
}
