<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->unsignedInteger('vendor_product_id')->index()->nullable();
            $table->integer('quantity')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_address_city')->nullable();
            $table->string('billing_address_country')->nullable();
            $table->string('billing_address_zip')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_address_city')->nullable();
            $table->string('shipping_address_country')->nullable();
            $table->string('shipping_address_zip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            //
        });
    }
}
