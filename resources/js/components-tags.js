Vue.component('sidebar', require('./components/common-components/SideBar.vue'));
Vue.component('customer-sidebar', require('./components/common-components/CustomerSideBar.vue'));
Vue.component('consumer-sidebar', require('./components/common-components/ConsumerSideBar.vue'));
Vue.component('admin-sidebar', require('./components/common-components/AdminSideBar.vue'));


Vue.component('top-header', require('./components/common-components/Header.vue'));
Vue.component('notification-block', require('./components/common-components/Notification.vue'));

//Dashboard
Vue.component('dashboard', require('./components/Dashboard.vue'));

// Form
Vue.component('form-elements-list', require('./components/Form.vue'));

// Table
Vue.component('table-list', require('./components/Table.vue'));

// Modal
Vue.component('modal', require('./components/Modal.vue'));

// Alert
Vue.component('alert', require('./components/Alert.vue'));

// Badges
Vue.component('badges', require('./components/Badges.vue'));

// Breadcrumbs
Vue.component('breadcrumbs', require('./components/Breadcrumbs.vue'));

// Button
Vue.component('button-element-list', require('./components/Button.vue'));

// Card
Vue.component('card', require('./components/Card.vue'));

// Nav
Vue.component('tabs', require('./components/Tabs.vue'));

// Avatar
Vue.component('avatar', require('./components/Avatar.vue'));

// Pagination
Vue.component('pagination', require('./components/Pagination.vue'));

// Rating
Vue.component('rating', require('./components/Rating.vue'));

// progress
Vue.component('progressbar', require('./components/Progress.vue'));

// Navs
Vue.component('navs', require('./components/Navs.vue'));

// Tooltip
Vue.component('tooltip', require('./components/Tooltip.vue'));

// Toasted
Vue.component('toasted', require('./components/Toasted.vue'));

// Login
Vue.component('login', require('./components/Login.vue'));

// Chat
Vue.component('chat-panel', require('./components/Chat.vue'));

// Vue Datepicker
Vue.component('vue-datepicker', require('./components/Datepicker.vue'));


// button loader
Vue.component('button-loader', require('./components/Loader.vue'));

Vue.component('common-methods', require('./components/CommonMethods.vue'));

Vue.component('file-upload', require('./components/common-components/FileUpload.vue'));
Vue.component('spinner-loader', require('./components/SpinnerLoader.vue'));


