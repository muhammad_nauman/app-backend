import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);

   
export default new Vuex.Store({

    
    // You can use it as state property
    state: {
        authUser : localStorage.getItem('user')?localStorage.getItem('user'):false,
        loginTab : false,
        forgotPassword : false,
        title: '',
       
    },

    // You can use it as a state getter function (probably the best solution)
    getters: {
        getAuthUser(state){
            return     JSON.parse(state.authUser);
        },
         getInitialiseStore(state){
            return localStorage.getItem('store', JSON.stringify(state));;
        },
        getLoginTab(state){
            return     state.loginTab;
        },
        getForgotPassword(state){
            return     state.forgotPassword;
        },
        getTitle(state){
            return     state.title;
        },
        getOfferName(state){
            return state.offerName;
        },
        getEstablishmentName(state){
            return state.establishmentName;
        }
    },

    // Mutation for when you use it as state property
    mutations: {
        setAuthUser(state, data){
           state.authUser = data;
           localStorage.setItem('user', JSON.stringify(data));
        },
        setLoginTab(state, data){
          state.loginTab = data;
        },
        setForgotPassword(state, data){
          state.forgotPassword = data;
        },
        setTitle(state, data){
          state.title = data;
        },
        setOfferName(state,data){
            state.offerName = data;
        },
        setEstablishmentName(state, data){
            state.establishmentName = data;
        }
    },
});