<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 Auth::routes();

// Categories route
Route::prefix('categories')->group(function() {
	Route::get('/', 'CategoriesController@get')->name('categories');
	Route::post('/create', 'CategoriesController@create')->name('create_category');
	Route::post('/{category}/update', 'CategoriesController@update')->name('update_category');
	Route::post('/{category}/delete', 'CategoriesController@delete')->name('delete_category');
	Route::post('/{category}/sub_category', 'CategoriesController@createSubCategory')->name('create_sub_category');
	Route::post('/{sub_category}/update', 'CategoriesController@updateSubCategory')->name('update_sub_category');
	Route::post('/{sub_category}/delete', 'CategoriesController@deleteSubCategory')->name('delete_sub_category');
});

// Categories route
Route::prefix('orders')->middleware('jwt.auth')->group(function() {
    Route::get('/consumer/products', 'OrderController@getProducts')->name('consumer.products');
});

Route::prefix('bookings')->middleware('jwt.auth')->group(function() {
    Route::get('/', 'BookingController@index');
    Route::post('create', 'BookingController@store');
    Route::get('mine', 'BookingController@myBookings');
    Route::post('{booking}/cancel', 'BookingController@cancel');
    Route::post('{id}/make/offer', 'BookingDetailController@makeOFfer');
    Route::post('buy', 'BookingDetailController@buy');
});


Route::prefix('vendors')->middleware('jwt.auth')->group(function() {
    Route::get('/products/{id}', 'VendorProductController@show');
});



Route::prefix('notifications')->middleware('jwt.auth')->group(function() {
    Route::get('/', 'NotificationController@get');
    Route::post('{notification}/read', 'NotificationController@read');
    Route::post('{notification}/delete', 'NotificationController@delete');
});

Route::prefix('chat')->middleware('jwt.auth')->group(function() {
    Route::get('/', 'ChatController@getChat');
    Route::post('send', 'ChatController@send');
    Route::get('{chat}/delete', 'ChatController@delete');
    Route::get('last-messages', 'ChatController@lastMessages');
});

Route::prefix('service_providers')->middleware('jwt.auth')->group(function() {
	Route::get('/', 'ServiceProvidersController@index');
	Route::post('create', 'ServiceProvidersController@store');
	Route::post('rate', 'ServiceProviderRatingController@create');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::get('seed', function () {
    \App\SubCategory::inRandomOrder()->limit(10)->get()->map(function($sub_category) use ($service_provider) {
        \Illuminate\Support\Facades\DB::insert("Insert into service_provider_sub_category (sub_category_id, service_provider_id) values ({$sub_category->id}, {$service_provider->id})");
    });
});

Route::post('file/upload', 'FileUploadController@upload')->name("file.upload");
Route::post('file/remove', 'FileUploadController@remove')->name("file.remove");


Route::post('login', 'Auth\LoginController@login');
